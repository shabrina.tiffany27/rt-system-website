import './App.css';
import Login from "./Components/Login";
import ForgotPass from "./Components/ForgotPass";
import Detail from "./Components/Detail";
import Informasi from "./Components/Informasi";
import Verifikasi from "./Components/Verifikasi";
import Ringkasan from "./Components/Ringkasan";
import Selesai from "./Components/Selesai";
import { Router, Route, Routes} from 'react-router-dom';

function App() {
  return (
      <div className="App">
        {/* <Routes>
          <Route exact path="/" element={<Login/>}/>
          <Route exact path="/ForgotPass" element={<ForgotPass/>}/>
        </Routes> */}
        <Selesai />
      </div>
  );
}

export default App;

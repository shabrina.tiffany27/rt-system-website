import React, { useState } from 'react';
import Login from '../Components/Login';
import ForgotPass from '../Components/ForgotPass';

export default function LoginPage() {
    const [step, setStep] = useState(0)

    const nextToForgotPass = () => {
        setStep(step + 1)
    }

    const nextToRegister = () => {
        setStep(step + 2)
    }

    const prev = () => {
        setStep(step - 1)
    }

    switch (step) {
        case 0:
            return (
                <Login nextToForgotPass={nextToForgotPass}
                    nextToRegister={nextToRegister}/>
            )
        case 1:
            return (
                <ForgotPass prev={prev}/>
            )
    }
}


import { TextField, FormLabel, FormControl} from '@mui/material';
import React, { Component } from 'react';

export default class Login extends Component {
    render (){
        return (
            <div>
                <div style={{backgroundColor:"#B7B7B7"}}>
                    <h2>Desa Sukacinta</h2>
                    <h2>Forgot Your Password?</h2>
                    <FormControl>
                        <FormLabel>No Handphone</FormLabel>
                        <TextField
                            placeholder='082125786647'
                            margin='normal'/>
                        <button>Reset Password</button>
                        <p>back to login</p> 
                    </FormControl>
                    
                </div>
            </div>
        )
    }
}
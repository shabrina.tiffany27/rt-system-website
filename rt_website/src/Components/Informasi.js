import { TextField, FormLabel, FormControl, MenuItem, Grid} from '@mui/material';
import React, { Component } from 'react';
// import { Link, BrowserRouter } from 'react-router-dom';

export default class Detail extends Component {
    render (){
        return (
            // <BrowserRouter>
            <div>
                <div style={{backgroundColor:"#B7B7B7"}}>
                    <h2>Register <span>Login <span>Layanan</span></span></h2>
                    <h2>Desa Sukacinta <span>Register</span></h2>
                    <h2>Selamat Datang di Website RW 001 Desa Sukacinta</h2>

                    <FormControl>
                        <FormLabel>Alamat Email</FormLabel>
                        <TextField
                            placeholder='Masukkan Alamat Email'
                            margin='normal'/>

                        <FormLabel>Password</FormLabel>
                        <TextField
                            placeholder='Masukkan Password'
                            margin='normal'/>

                        <FormLabel>Ulangi Password</FormLabel>
                        <TextField
                            placeholder='Ulangi Password'
                            margin='normal'/>

                        <FormLabel>Upload KTP</FormLabel>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            placeholder='Format .jpg, .png max size 1 MB'
                            />

                        <button>Simpan</button>
                        <button>Kembali</button>
                        <button>Lanjut</button>
                    </FormControl>  
                </div>
            </div>
            // </BrowserRouter>
            
            
        )
    }
}
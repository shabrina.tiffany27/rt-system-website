import { TextField, FormLabel, FormControl, MenuItem, Grid, FormControlLabel, Checkbox} from '@mui/material';
import React, { Component } from 'react';
import HCaptcha from "@hcaptcha/react-hcaptcha";
// import { Link, BrowserRouter } from 'react-router-dom';

export default function Ringkasan () {
    const sitekey = "e76d04b5-85fd-4c27-b158-d521e16e1382";

    // const handleVerificationSuccess = async response => {
    //     // note - if it wreen't for CORS this woulld work
    //     var bodyFormData = new FormData();
    //     console.log(bodyFormData)
    //     bodyFormData.set("secret", sitekey);
    //     bodyFormData.set("response", response);
    //     try {
    //         const { data } = await rexter.post(
    //             "https://hcaptcha.com/siteverify",
    //             bodyFormData,
    //             {
    //                 headers: {
    //                     "Content-Type": "application/x-www-form-urlencoded"
    //                 },
    //                 crossdomain: true
    //             }
    //         );
    //         console.log("server response:", data);
    //         //setValidation(data);
    //         // this.setState({ captchaValid: data });
    //         setCaptchaValid(data)
    //     } catch (err) {
    //         // console.log("CANNOT SUBMIT:", response, err);
    //         // this.setState({ captchaValid: true})
    //         setCaptchaValid(true)
    //     }
    //     console.log(captchaValid)
    // };
    return (
        
            // <BrowserRouter>
            <div>
                <div style={{backgroundColor:"#B7B7B7"}}>
                    <h2>Register <span>Login <span>Layanan</span></span></h2>
                    <h2>Desa Sukacinta </h2>
                    <h2>Selamat Datang di Website RW 001 Desa Sukacinta</h2>
                    <h2>Ringkasan</h2>

                    <FormControl>
                        <FormLabel>NIK</FormLabel>
                        <TextField
                            margin='normal'/>

                        <FormLabel>Nama Lengkap</FormLabel>
                        <TextField
                            margin='normal'/>

                        <FormLabel>Jenis Kelamin</FormLabel>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            />

                        <FormLabel>Agama</FormLabel>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            />

                        <FormLabel>Kewarganegaraan</FormLabel>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            />

                        <FormLabel>Alamat Email</FormLabel>
                        <TextField
                            variant="outlined"
                            margin="normal"/>

                        <FormLabel>Upload KTP</FormLabel>
                        <TextField
                            variant="outlined"
                            margin="normal"/>

                        <FormLabel>No Hp</FormLabel>
                        <TextField
                            variant="outlined"
                            margin="normal"/>

                        <p >
                            <Grid>
                                <Grid >
                                    <Checkbox
                                        disableRipple
                                        color="default"
                                    />
                                </Grid>
                                <Grid >
                                    <label>
                                        Semua informasi dan dokumen yang saya lampirkan dalam permohonan ini
                                        adalah benar dan apabila terdapat perubahan data dalam aplikasi,
                                        saya wajib segera memberikan informasi terbaru kepada Bank Muamalat.
                                    </label>
                                </Grid>
                            </Grid>
                        </p>


                        <HCaptcha
                            sitekey={sitekey}
                            // onVerify={token => this.handleVerificationSuccess(token)}
                            // onVerify={handleVerificationSuccess}
                        />

                        <button>Kembali</button>
                        <button>Lanjut</button>
                    </FormControl>  
                </div>
            </div>
            // </BrowserRouter>
            
            
    )
}
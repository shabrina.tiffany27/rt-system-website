import { TextField, FormLabel, FormControl, MenuItem, Grid} from '@mui/material';
import React, { Component } from 'react';
import OtpInput from 'react-otp-input';
// import { Link, BrowserRouter } from 'react-router-dom';

export default class Detail extends Component {
    render (){
        return (
            // <BrowserRouter>
            <div>
                <div style={{backgroundColor:"#B7B7B7"}}>
                    <h2>Register <span>Login <span>Layanan</span></span></h2>
                    <h2>Desa Sukacinta <span>Register</span></h2>
                    <h2>Selamat Datang di Website RW 001 Desa Sukacinta</h2>

                    <FormControl>
                        <FormLabel>No Hp</FormLabel>
                        <TextField
                            placeholder='85456789'
                            margin='normal'/>


                        <Grid>
                            <form>
                                <div>
                                    <p>Masukkan OTP</p>
                                    <p>Masukkan 6 Digit kode verifikasi yang telah dikirimkan via no. handphone</p>
                                </div>
                            
                                <OtpInput
                                    numInputs={6}
                                    // separator={<span>-</span>}
                                    disabled={false}/>
                            </form>
                            <p>Belum menerima kode? <span>Kirim Ulang</span></p>
                        </Grid>

                        <button>Simpan</button>
                        <button>Kembali</button>
                        <button>Lanjut</button>
                    </FormControl>  
                </div>
            </div>
            // </BrowserRouter>
            
            
        )
    }
}
import { TextField, FormLabel, FormControl, MenuItem, Grid} from '@mui/material';
import React, { Component } from 'react';
// import { Link, BrowserRouter } from 'react-router-dom';

export default class Detail extends Component {
    render (){
        return (
            // <BrowserRouter>
            <div>
                <div style={{backgroundColor:"#B7B7B7"}}>
                    <h2>Register <span>Login <span>Layanan</span></span></h2>
                    <h2>Desa Sukacinta <span>Register</span></h2>
                    <h2>Selamat Datang di Website RW 001 Desa Sukacinta</h2>

                    <FormControl>
                        <FormLabel>NIK</FormLabel>
                        <TextField
                            placeholder='Masukkan NIK'
                            margin='normal'/>

                        <FormLabel>Nama Lengkap</FormLabel>
                        <TextField
                            placeholder='Masukkan Nama Lengkap'
                            margin='normal'/>

                        <FormLabel>Tempat Lahir</FormLabel>
                        <TextField
                            placeholder='Masukkan Tempat Lahir'
                            margin='normal'/>

                        <FormLabel>Tanggal Lahir</FormLabel>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            type="date"
                            />


                        <FormLabel>Jenis Kelamin</FormLabel>
                        <TextField
                            placeholder='Pilih Jenis Kelamin'
                            variant="outlined"
                            margin="normal"
                            select
                            >
                            <MenuItem value="Pria">Pria</MenuItem>
                            <MenuItem value="Wanita">Wanita</MenuItem>
                        </TextField>

                        <FormLabel>Agama</FormLabel>
                        <TextField
                            placeholder='Pilih Agama'
                            variant="outlined"
                            margin="normal"
                            select
                            >
                            <MenuItem value="Islam">Islam</MenuItem>
                            <MenuItem value="Katolik">Katolik</MenuItem>
                            <MenuItem value="Protestan">Protestan</MenuItem>
                            <MenuItem value="Hindu">Hindu</MenuItem>
                            <MenuItem value="Budha">Budha</MenuItem>
                            <MenuItem value="Konghucu">Konghucu</MenuItem>
                        </TextField>

                        <FormLabel>Kewarganegaraan</FormLabel>
                        <TextField
                            placeholder='Pilih Kewarganegaraan'
                            variant="outlined"
                            margin="normal"
                            select
                            >
                            <MenuItem value="Indonesia">Indonesia</MenuItem>
                            <MenuItem value="Asing">Asing</MenuItem>
                        </TextField>


                        <FormLabel>Alamat Tempat Tinggal</FormLabel>
                        <Grid>
                            <Grid>
                                <label className="basicLabel">Provinsi</label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    placeholder='Pilih Provinsi'
                                    select>
                                        <MenuItem value='a'>Pilih Provinsi</MenuItem>
                                </TextField>
                            </Grid>
                            <Grid >
                                <label className="basicLabel">Kota/Kabupaten</label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    placeholder="Pilih Kota"
                                    select>
                                        <MenuItem value='b'> Pilih Kota </MenuItem>
                                </TextField>
                            </Grid>
                        </Grid>

                        <Grid>
                            <Grid>
                                <label className="basicLabel">Kecamatan</label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    placeholder='Pilih Kecamatan'
                                    select>
                                        <MenuItem value='c'>Pilih Kecamatan</MenuItem>
                                </TextField>
                            </Grid>
                            <Grid >
                                <label className="basicLabel">Kelurahan</label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    placeholder="Pilih Kelurahan"
                                    select>
                                        <MenuItem value='d'> Pilih Kelurahan </MenuItem>
                                </TextField>
                            </Grid>
                        </Grid>

                        <Grid>
                            <Grid>
                                <label className="basicLabel">Kode Pos</label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    placeholder='Pilih Kode Pos'
                                    select>
                                        <MenuItem value='e'>Pilih Kode Pos</MenuItem>
                                </TextField>
                            </Grid>
                            <Grid >
                                <label className="basicLabel">RT</label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    placeholder="Ketik RT"/>
                            </Grid>
                            <Grid >
                                <label className="basicLabel">RW</label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    placeholder="Ketik RW"/>
                            </Grid>
                        </Grid>

                        <FormLabel>Nama Jalan</FormLabel>
                        <TextField
                            placeholder='Ketik Nama Jalan'
                            variant="outlined"
                            margin="normal"/>

                        <button>Simpan</button>
                        <button>Kembali</button>
                        <button>Lanjut</button>
                    </FormControl>  
                </div>
            </div>
            // </BrowserRouter>
            
            
        )
    }
}